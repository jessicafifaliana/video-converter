package edu.esipe.i3.ezipflix.frontend.pubsub;

import com.google.cloud.pubsub.v1.AckReplyConsumer;
import edu.esipe.i3.ezipflix.frontend.VideoDispatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gcp.pubsub.core.PubSubOperations;
import org.springframework.cloud.gcp.pubsub.integration.AckMode;
import org.springframework.cloud.gcp.pubsub.integration.inbound.PubSubInboundChannelAdapter;
import org.springframework.cloud.gcp.pubsub.integration.outbound.PubSubMessageHandler;
import org.springframework.cloud.gcp.pubsub.support.GcpPubSubHeaders;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class GooglePubSubController {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(GooglePubSubController.class, args);
    }

    @Bean
    public MessageChannel pubSubInputChannel(){
        DirectChannel d = new DirectChannel();
        System.out.println("channel created !!!!!!!!!! bean name = " + d);
        return d;
    }
    // Create PubSubInboundChannelAdapter Bean for messageChannelAdapter
    @Bean
    public PubSubInboundChannelAdapter messageChannelAdapter(
            @Qualifier("pubSubInputChannel") MessageChannel inputChannel, PubSubOperations pubSubTemplate) {
        PubSubInboundChannelAdapter adapter = new PubSubInboundChannelAdapter(pubSubTemplate, "video_conv_sub");
        adapter.setOutputChannel(inputChannel);
        adapter.setAckMode(AckMode.MANUAL);
        return adapter;
    }

    // Create Service Activator Bean for Pub sub input channel
    @Bean
    @ServiceActivator(inputChannel = "pubsubInputChannel")
    public MessageHandler messageReceiver() {
        return message -> {
            System.out.println("Message Received::" + message.getPayload());
            AckReplyConsumer consumer = (AckReplyConsumer) message.getHeaders().get(GcpPubSubHeaders.ORIGINAL_MESSAGE);
            consumer.ack();
        };
    }

    // Create Service Activator Bean for Pub sub output channel
    @Bean
    @ServiceActivator(inputChannel = "pubsubOutputChannel")
    public MessageHandler messageSender(PubSubOperations pubsubTemplate) {
        return new PubSubMessageHandler(pubsubTemplate, "video_cgonversion");
    }
    //private PubsubOutboundGateway messageGateway;

    @RequestMapping(value = "/publish", method = RequestMethod.POST)
    public String publishMessage(@RequestBody MyAppGCPMessage message) {
        PubsubOutboundGateway.sendToPubsub(message.toString());
        return "Your message was :"+ message.toString();
    }

}