package edu.esipe.i3.ezipflix.frontend;

import com.google.cloud.pubsub.v1.AckReplyConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gcp.pubsub.core.PubSubOperations;
import org.springframework.cloud.gcp.pubsub.integration.AckMode;
import org.springframework.cloud.gcp.pubsub.integration.inbound.PubSubInboundChannelAdapter;
import org.springframework.cloud.gcp.pubsub.integration.outbound.PubSubMessageHandler;
import org.springframework.cloud.gcp.pubsub.support.GcpPubSubHeaders;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Gilles GIRAUD gil on 11/4/17.
 */

//@SpringBootApplication
//@RestController
//@EnableRabbit
//@EnableWebSocket
public class VideoDispatcher
        //implements WebSocketConfigurer
        {

    // rabbitmqadmin -H localhost -u ezip -p pize -V ezip delete queue name=video-conversion-queue
    // rabbitmqadmin -H localhost -u ezip -p pize -V ezip delete exchange name=video-conversion-exchange
    // sudo rabbitmqadmin -u ezip -p pize -V ezip declare exchange name=video-conversion-exchange type=direct
    // sudo rabbitmqadmin -u ezip -p pize -V ezip declare queue name=video-conversion-queue durable=true
    // sudo rabbitmqadmin -u ezip -p pize -V ezip declare binding source="video-conversion-exchange" destination_type="queue" destination="video-conversion-queue" routing_key="video-conversion-queue"
    // MONGO : db.video_conversions.remove({})

    //sudo rabbitmq-server start
    private static final Logger LOGGER = LoggerFactory.getLogger(VideoDispatcher.class);

/*    @Value("${rabbitmq-server.credentials.username}") private String username;
    @Value("${rabbitmq-server.credentials.password}") private String password;
    @Value("${rabbitmq-server.credentials.vhost}") private String vhost;
    @Value("${rabbitmq-server.server}") private String host;
    @Value("${rabbitmq-server.port}") private String port;
    @Value("${conversion.messaging.rabbitmq.conversion-queue}") public  String conversionQueue;
    @Value("${conversion.messaging.rabbitmq.conversion-exchange}") public  String conversionExchange;*/

    //@Autowired VideoConversion videoConversion;
//    public static void main(String[] args) throws Exception {
//        SpringApplication.run(VideoDispatcher.class, args);
//    }

    // ┌───────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
    // │ REST Resources                                                                                                │
    // └───────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
   /* @RequestMapping(method = RequestMethod.POST,
                    value = "/convert",
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public ConversionResponse requestConversion(@RequestBody ConversionRequest request) throws JsonProcessingException {
        LOGGER.info("File = {}", request.getPath());
        final ConversionResponse response = new ConversionResponse();
        LOGGER.info("UUID = {}", response.getUuid().toString());
        videoConversion.save(request, response);
        return response;
    }
    @Bean
    public WebSocketHandler videoStatusHandler() {
        return new VideoStatusHandler();
    }

    public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {
        webSocketHandlerRegistry.addHandler(videoStatusHandler(), "/video_status");
    }*/

//A COMMENTER
//    @Bean
//    AmqpAdmin amqpAdmin() {
//        RabbitAdmin rabbitAdmin = new RabbitAdmin(connectionFactory());
//        q = rabbitAdmin.declareQueue(new Queue(conversionQueue));
//        rabbitAdmin.declareExchange(new DirectExchange(conversionExchange));
//        Binding binding = BindingBuilder.bind(new Queue(conversionQueue)).to(new DirectExchange(conversionExchange))
//                .with(COMMANDS_QUEUE);
//        rabbitAdmin.declareBinding(binding);
//
//        rabbitAdmin.setAutoStartup(true);
//        return rabbitAdmin;
//    }

//    @Bean(name="video-conversion-template")
//    public RabbitTemplate getVideoConversionTemplate() {
//        RabbitTemplate template = new RabbitTemplate(connectionFactory());
//
//        template.setExchange(conversionExchange);
//        template.setRoutingKey(conversionQueue);
//        template.setQueue(conversionQueue);
//        return template;
//    }
    //pubsub
    //Create Message Channel bean for pubsub bean
    /*@Bean
    public MessageChannel pubSubInputChannel(){
        DirectChannel d = new DirectChannel();
        System.out.println("channel created !!!!!!!!!! bean name = " + d);
        return d;
    }
    // Create PubSubInboundChannelAdapter Bean for messageChannelAdapter
    @Bean
    public PubSubInboundChannelAdapter messageChannelAdapter(
            @Qualifier("pubSubInputChannel") MessageChannel inputChannel, PubSubOperations pubSubTemplate) {
        PubSubInboundChannelAdapter adapter = new PubSubInboundChannelAdapter(pubSubTemplate, "video_conv_sub");
        adapter.setOutputChannel(inputChannel);
        adapter.setAckMode(AckMode.MANUAL);
        return adapter;
    }

    // Create Service Activator Bean for Pub sub input channel
    @Bean
    @ServiceActivator(inputChannel = "pubsubInputChannel")
    public MessageHandler messageReceiver() {
        return message -> {
            System.out.println("Message Received::" + message.getPayload());
            AckReplyConsumer consumer = (AckReplyConsumer) message.getHeaders().get(GcpPubSubHeaders.ORIGINAL_MESSAGE);
            consumer.ack();
        };
    }

    // Create Service Activator Bean for Pub sub output channel
    @Bean
    @ServiceActivator(inputChannel = "pubsubOutputChannel")
    public MessageHandler messageSender(PubSubOperations pubsubTemplate) {
        return new PubSubMessageHandler(pubsubTemplate, "video_conversion");
    }*/

    // Create Messaging Gateway interface to publish the message
/*    @MessagingGateway(defaultRequestChannel = "pubsubOutputChannel")
    public interface PubsubOutboundGateway {
        void sendToPubsub(String text);
    }*/

}
